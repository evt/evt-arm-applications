/* 
 * File:   app.c
 * Author: Derek Gutheil <dkg8689@rit.edu>
 *
 * Created on September 8, 2016, 8:07 PM
 */

#include "app.h"
#include "app_network.h"
#include "app_temp.h"
#include "app_current.h"
#include "app_network_stateMachine.h"
#include "app_voltages.h"


void app_init()
{
    //Initialize the app layer
    app_network_init();
}

void app_1ms()
{
    //Run any app specific 1ms cyclic tasks

}

void app_10ms()
{
    //Run any app specific 10ms cyclic tasks
    app_current_calcCurrent();
    app_network();
    app_network_10ms();
}

void app_100ms()
{
    /* ===========================================================================================================================
     * Run the apps
     * ===========================================================================================================================
    */
    app_network();
    app_temp_updateTemps();
    
    /* ===========================================================================================================================
     * Run the app state machines
     * ===========================================================================================================================
    */
    //Run the networkManager state machine
    app_network_sm();
    //Run any app specific 100ms cyclic tasks
    app_voltages_updateCellVoltages();
    app_network_100ms();
}


void app_1000ms()
{
    /* ===========================================================================================================================
     * Run the apps
     * ===========================================================================================================================
    */
    app_network_1000ms();
}