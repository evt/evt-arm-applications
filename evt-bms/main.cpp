#include <stdio.h>
#include <stdlib.h>
#include "diag/Trace.h"
#include "stm32f3xx.h"


void systemClockInit();
void gpioInit();


int main(int argc, char** argv)
{
	systemClockInit();
	gpioInit();

	// Infinite loop
	while (1)
	{
		// read button
		if (!READ_BIT(GPIOC->IDR, GPIO_IDR_13))
		{
			// if button pressed, turn on LED
			//CLEAR_BIT(GPIOA->ODR, GPIO_ODR_5);
			//CLEAR_BIT(GPIOA->BSRR, GPIO_BSRR_BR_5);
			SET_BIT(GPIOA->ODR, GPIO_ODR_5);
		}
		else
		{
			// else turn off LED
			//SET_BIT(GPIOA->ODR, GPIO_ODR_5);
			//CLEAR_BIT(GPIOA->BSRR, GPIO_BSRR_BS_5);
			CLEAR_BIT(GPIOA->ODR, GPIO_ODR_5);
		}
		//asm volatile("nop\r\n");
	}
}


void systemClockInit()
{
	// set up clocks
	// wait until the internal oscillator is ready
	while(!READ_BIT(RCC->CR, RCC_CR_HSIRDY))
	{
	  asm volatile("nop\r\n");
	}
	// then enable it
	SET_BIT(RCC->CR, RCC_CR_HSION);
	// enable the PLL
	SET_BIT(RCC->CR, RCC_CR_PLLON);
	// configure PLL source as HSI/2
	CLEAR_BIT(RCC->CFGR, RCC_CFGR_PLLSRC);
	// multiply the PLL input by 2 (equals HSI/2*2 = HSI)
	SET_BIT(RCC->CFGR, RCC_CFGR_PLLMUL_3);
	SET_BIT(RCC->CFGR, RCC_CFGR_PLLMUL_2);
	SET_BIT(RCC->CFGR, RCC_CFGR_PLLMUL_1);
	SET_BIT(RCC->CFGR, RCC_CFGR_PLLMUL_0);

	// sysclock is sourced from pllclk
	SET_BIT(RCC->CFGR, RCC_CFGR_SW_1);
	CLEAR_BIT(RCC->CFGR, RCC_CFGR_SW_0);
	// do not divide for HCLK
	CLEAR_BIT(RCC->CFGR, RCC_CFGR_HPRE_3);
	// do not divide for PCLK1
	SET_BIT(RCC->CFGR, RCC_CFGR_PPRE1_2);
	CLEAR_BIT(RCC->CFGR, RCC_CFGR_PPRE1_1);
	CLEAR_BIT(RCC->CFGR, RCC_CFGR_PPRE1_0);
	// do not divide for PCLK2
	CLEAR_BIT(RCC->CFGR, RCC_CFGR_PPRE2_2);
	// set the flash latency
	// for our HCLK = 64 MHz, set to 010
	CLEAR_BIT(FLASH->ACR, FLASH_ACR_LATENCY_2);
	SET_BIT(FLASH->ACR, FLASH_ACR_LATENCY_1);
	CLEAR_BIT(FLASH->ACR, FLASH_ACR_LATENCY_0);

	return;
}


void gpioInit()
{
	// set up GPIO

	// enable GPIOA and GPIOC clock
	SET_BIT(RCC->AHBENR, RCC_AHBENR_GPIOAEN);
	SET_BIT(RCC->AHBENR, RCC_AHBENR_GPIOCEN);
	// set one to input
	CLEAR_BIT(GPIOC->MODER, GPIO_MODER_MODER13_0);
	CLEAR_BIT(GPIOC->MODER, GPIO_MODER_MODER13_1);
	// set another to output
	SET_BIT(GPIOA->MODER, GPIO_MODER_MODER5_0);
	CLEAR_BIT(GPIOA->MODER, GPIO_MODER_MODER5_1);
	// set output to push-pull
	CLEAR_BIT(GPIOA->OTYPER, GPIO_OTYPER_OT_5);
	// set input with pull-up
	SET_BIT(GPIOC->PUPDR, GPIO_PUPDR_PUPDR13_1);
	CLEAR_BIT(GPIOC->PUPDR, GPIO_PUPDR_PUPDR13_0);
}

