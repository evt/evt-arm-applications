/* 
 * File:   dev.h
 * Author: Derek Gutheil <dkg8689@rit.edu>
 *
 * Created on September 8, 2016, 8:07 PM
 */

#ifndef DEV_H
#define    DEV_H

void dev_init();
void dev_1ms();
void dev_10ms();
void dev_100ms();

#endif    /* DEV_H */