/* 
 * File:   IO.h
 * Author: Derek Gutheil <dkg8689@rit.edu>
 *
 * Created on September 8, 2016, 8:07 PM
 */

#ifndef IO_H
#define IO_H

/* Called automatically from taskManager on boot */
void IO_init();

/* Called automatically from taskManager on a 1ms period */
void IO_1ms();

/* Called automatically from taskManager on a 10ms period */
void IO_10ms();

/* Called automatically from taskManager on a 100ms period */
void IO_100ms();

#endif    /* IO_H */