/* 
 * File:   MC_processor.h
 *
 * Created on December 8, 2016, 1:01 PM
 */

#ifndef MC_PROCESSOR_H
#define	MC_PROCESSOR_H

/* 
 * XC is the name of the compiler. Using project settings it goes down and selects the correct processor and compiler includes for this PIC
 * It also defines _18F25K80 (Or maybe the project itself does)
 */
#include <xc.h>

#endif	/* MC_PROCESSOR_H */

