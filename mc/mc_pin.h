/*
 * file: mc_pin.h
 */

#ifndef MC_PIN_H
#define MC_PIN_H

#include "stdint.h"

namespace MC {

enum class PORT {
   A,
   B,
   C,
   D,
};

struct PIN {
   PORT port;
   uint8_t pin_number;
};

constexpr PIN PA0 = {PORT::A, 0};
constexpr PIN PA1 = {PORT::A, 1};
constexpr PIN PA2 = {PORT::A, 2};
constexpr PIN PA3 = {PORT::A, 3};
constexpr PIN PA4 = {PORT::A, 4};
constexpr PIN PA5 = {PORT::A, 5};
constexpr PIN PA6 = {PORT::A, 6};
constexpr PIN PA7 = {PORT::A, 7};
constexpr PIN PA8 = {PORT::A, 8};
constexpr PIN PA9 = {PORT::A, 9};
constexpr PIN PA10 = {PORT::A, 10};
constexpr PIN PA11 = {PORT::A, 11};
constexpr PIN PA12 = {PORT::A, 12};
constexpr PIN PA13 = {PORT::A, 13};
constexpr PIN PA14 = {PORT::A, 14};
constexpr PIN PA15 = {PORT::A, 15};
constexpr PIN PB0 = {PORT::B, 0};
constexpr PIN PB1 = {PORT::B, 1};
constexpr PIN PB2 = {PORT::B, 2};
constexpr PIN PB3 = {PORT::B, 3};
constexpr PIN PB4 = {PORT::B, 4};
constexpr PIN PB5 = {PORT::B, 5};
constexpr PIN PB6 = {PORT::B, 6};
constexpr PIN PB7 = {PORT::B, 7};
constexpr PIN PB8 = {PORT::B, 8};
constexpr PIN PB9 = {PORT::B, 9};
constexpr PIN PB10 = {PORT::B, 10};
constexpr PIN PB11 = {PORT::B, 11};
constexpr PIN PB12 = {PORT::B, 12};
constexpr PIN PB13 = {PORT::B, 13};
constexpr PIN PB14 = {PORT::B, 14};
constexpr PIN PB15 = {PORT::B, 15};
constexpr PIN PC0 = {PORT::C, 0};
constexpr PIN PC1 = {PORT::C, 1};
constexpr PIN PC2 = {PORT::C, 2};
constexpr PIN PC3 = {PORT::C, 3};
constexpr PIN PC4 = {PORT::C, 4};
constexpr PIN PC5 = {PORT::C, 5};
constexpr PIN PC6 = {PORT::C, 6};
constexpr PIN PC7 = {PORT::C, 7};
constexpr PIN PC8 = {PORT::C, 8};
constexpr PIN PC9 = {PORT::C, 9};
constexpr PIN PC10 = {PORT::C, 10};
constexpr PIN PC11 = {PORT::C, 11};
constexpr PIN PC12 = {PORT::C, 12};
constexpr PIN PC13 = {PORT::C, 13};
constexpr PIN PC14 = {PORT::C, 14};
constexpr PIN PC15 = {PORT::C, 15};
constexpr PIN PD0 = {PORT::D, 0};
constexpr PIN PD1 = {PORT::D, 1};
constexpr PIN PD2 = {PORT::D, 2};
constexpr PIN PD3 = {PORT::D, 3};
constexpr PIN PD4 = {PORT::D, 4};
constexpr PIN PD5 = {PORT::D, 5};
constexpr PIN PD6 = {PORT::D, 6};
constexpr PIN PD7 = {PORT::D, 7};
constexpr PIN PD8 = {PORT::D, 8};
constexpr PIN PD9 = {PORT::D, 9};
constexpr PIN PD10 = {PORT::D, 10};
constexpr PIN PD11 = {PORT::D, 11};
constexpr PIN PD12 = {PORT::D, 12};
constexpr PIN PD13 = {PORT::D, 13};
constexpr PIN PD14 = {PORT::D, 14};
constexpr PIN PD15 = {PORT::D, 15};

}

#endif // MC_PIN_H
