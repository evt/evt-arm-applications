/*
 * File:    MC_CAN.h
 * Date:    1/7/16
 */

#ifndef MC_CAN_H
#define MC_CAN_H

#include "EVT_std_includes.h"
#include "MC_register_defaults.h"

/* ===========================================================================================================================
 * Public precompiler definitions
 * ===========================================================================================================================
*/

/* ===========================================================================================================================
 * Public Enumerated type definitions
 * ===========================================================================================================================
 */
typedef enum mc_can_baud_E
{
    MC_CAN_BAUD_DEFAULT,
    MC_CAN_BAUD_125K,
    MC_CAN_BAUD_250K,
    MC_CAN_BAUD_500K,
    MC_CAN_BAUD_1M,
} mc_can_baud_E;

/* ============== This section is dedicated to setting up the operational mode register ECANCON (see page 439 of Datasheet)=====================*/

/* Mode 0 - Legacy Mode is designed to be fully compatible with CAN modules used in PIC18CXX8 and PIC18FXX8 devices.
 * This is the default mode of operation on all Reset conditions. As a result, module code written for the
 * PIC18XX8 CAN module may be used on the ECAN module without any code changes.
 * The following is the list of resources available in Mode 0:
 *    - Three transmit buffers: TXB0, TXB1 and TXB2
 *    - Two receive buffers: RXB0 and RXB1
 *    - Two acceptance masks, one for each receive buffer: RXM0, RXM1
 *    - Six acceptance filters, 2 for RXB0 and 4 for RXB1: RXF0, RXF1, RXF2, RXF3, RXF4, RXF5
 *
 */

/* Mode 1 - Enhanced Legacy Mode is similar to Mode 0, with the exception that more resources are available in Mode 1.
 * There are 16 acceptance filters and two acceptance mask registers. Acceptance Filter 15 can be used as either an
 * acceptance filter or an acceptance mask register. In addition to three transmit and two receive buffers, there
 * are six more message buffers. One or more of these additional buffers can be programmed as transmit or
 * receive buffers. These additional buffers can also be programmed to automatically handle RTR messages.
 * The following is the list of resources available in Mode 1:
 *    - Three transmit buffers: TXB0, TXB1 and TXB2
 *    - Two receive buffers: RXB0 and RXB1
 *    - Six buffers programmable as TX or RX: B0-B5
 *    - Automatic RTR handling on B0-B5
 *    - Sixteen dynamically assigned acceptance filters: RXF0-RXF15
 *    - Two dedicated acceptance mask registers; RXF15 programmable as third mask: RXM0-RXM1, RXF15
 *    - Programmable data filter on standard identifier messages: SDFLC
 *
 */

/* In Mode 2 -  Enhanced FIFO Mode, two or more receive buffers are used to form the receive FIFO (first in, first out) buffer. There is no
 * one-to-one relationship between the receive buffer and acceptance filter registers. Any filter that is enabled and
 * linked to any FIFO receive buffer can generate acceptance and cause FIFO to be updated.
 * FIFO length is user-programmable, from 2-8 buffers deep. FIFO length is determined by the very first
 * programmable buffer that is configured as a transmit buffer. For example, if Buffer 2 (B2) is programmed as a
 * transmit buffer, FIFO consists of RXB0, RXB1, B0 and B1, creating a FIFO length of 4. If all programmable
 * buffers are configured as receive buffers, FIFO will have the maximum length of 8.
 * The following is the list of resources available in Mode 2:
 *    - Three transmit buffers: TXB0, TXB1 and TXB2
 *    - Two receive buffers: RXB0 and RXB1
 *    - Six buffers programmable as TX or RX; receive buffers form FIFO: B0-B5
 *    - Automatic RTR handling on B0-B5
 *    - Sixteen acceptance filters: RXF0-RXF15
 *    - Two dedicated acceptance mask registers; RXF15 programmable as third mask: RXM0-RXM1, RXF15
 *    - Programmable data filter on standard identifier messages: SDFLC, useful for DeviceNet protocol
 */
/* =============================================================================================================================================*/

typedef enum mc_can_mode_E
{
    MC_CAN_LEGACY_MODE          = 0u,
    MC_CAN_ENHANCE_LEGACY_MODE  = 1u,
    MC_CAN_ENHANCED_FIFO_MODE   = 2u,
} mc_can_mode_E;

/* ===========================================================================================================================
 * Public Structure definitions
 * ===========================================================================================================================
 */
typedef struct mc_can_frame_S
{
    /* We can NOT use a bitfield union here as
     * "If insufficient space remains, whether a bit-field that does not fit is put into the next unit or overlaps adjacent units is implementation-defined." - C standard section 6.7.2.1
     * But it does not appear that XC8 allows using bitfields across storage boundary
     */
    uint16_t address;    
    uint8_t data[8];
    /* numBytes must be <= 8! */
    uint8_t numBytes             :4;
    /*This field is internal, does not affect the structure of the message itself, only affects transmit priority*/
    uint8_t priority             :2;
} mc_can_frame_S;

typedef union mc_can_receive_pending_S
{
    struct
    {
        uint8_t pending;
    };
    struct
    {
        uint8_t RXB0     :1;
        uint8_t RXB1     :1;
        uint8_t RXB2     :1;
        uint8_t RXB3     :1;
        uint8_t RXB4     :1;
        uint8_t RXB5     :1;
        uint8_t RXB6     :1;
        uint8_t RXB7     :1;
    };
} mc_can_receive_pending_S;

/* ===========================================================================================================================
 * Public function declarations
 * ===========================================================================================================================
 */

/* These 2 functions set the initialization parameters of the CAN module.
 * These MUST be set before the MC_CAN_INIT() gets called by the task manager
 */
void mc_can_setRequestedBaudrate(mc_can_baud_E baudrate);
void mc_can_setRequestedOperatingMode(mc_can_mode_E operatingMode);

/* mc_can_transmit()
 * This function initiates the immediate transmission of a CAN frame
 * Note, this is NOT a queued function, the transmission happens immediately
 */
uint8_t mc_can_transmit(mc_can_frame_S * frame);

/* mc_can_getMessage() 
 * This function returns (via pointer) a CAN message from the bus
 * Note, this is a queued function. CAN messages are received via interrupt and stored in this layer.
 * This function dequeues the next message and returns it. Should be run continuously until the received message buffer is empty
 */
void mc_can_getMessage(mc_can_frame_S * frameOut);

/* mc_can_bufferIsEmpty() 
 * This function returns the status of the can receive buffer
 *    1 = buffer is empty
 *    0 = buffer is not empty
 */
uint8_t mc_can_bufferIsEmpty();

/*  ========================== Functions for the interrupt handler ONLY =============================================================*/

/* mc_can_init()
 * Sets the CAN module up as defined by variables set via MC_CAN_setup().
 * SHOULD ONLY BE CALLED BY TASK MANAGER.
 * Must be called after setRequests are done by the IO layer
 */
void mc_can_init();

/* mc_can_getRXBuffer'n'()
 * Retrieves the can frame from buffer 'n' and stores it in the FIFO Queue
 */
void mc_can_getRXBuffer0();
void mc_can_getRXBuffer1();
void mc_can_getRXBuffer2();
void mc_can_getRXBuffer3();
void mc_can_getRXBuffer4();
void mc_can_getRXBuffer5();
void mc_can_getRXBuffer6();
void mc_can_getRXBuffer7();
/*  =================================================================================================================================*/

#endif  /* MC_CAN_H */
