/* 
 * File:   MC_GPIO.c
 *
 * Created on October 13, 2016
 */

#include "MC_GPIO.h"
#include "MC_processor.h"

/* ===========================================================================================================================
 * Private module variables
 * ===========================================================================================================================
*/

/* ===========================================================================================================================
 * Private module function declarations
 * ===========================================================================================================================
*/

/* ===========================================================================================================================
 *  Module function definitions
 * ===========================================================================================================================
*/

int8_t MC_GPIO_write(MC_PIN_E pin, MC_PIN_STATES_E value)
{
    if (MC_pin_getDirection(pin) != MC_PIN_OUTPUT)
    {
        return -1;
    }
    else if (value >= MC_PIN_NUM_PIN_STATES)
    {
        return -1;
    }
    else if (pin >= MC_PIN_NUM_PINS)
    {
        return -1;
    }
    else
    {
        #ifdef _18F25K80
        switch(pin)
        {
            case MC_PIN_NULL:
                return -1;
            case MC_PIN_RE3:
                return -1;
            case MC_PIN_RA0:
                LATAbits.LATA0 = value;
                break;      
            case MC_PIN_RA1:
                LATAbits.LATA1 = value;
                break;
            case MC_PIN_RA2:       
                LATAbits.LATA2 = value;
                break;
            case MC_PIN_RA3:       
                LATAbits.LATA3 = value;
                break;
            case MC_PIN_6:       
                return -1;
            case MC_PIN_RA5:       
                LATAbits.LATA5 = value;
                break;
            case MC_PIN_8:
                return -1;
            case MC_PIN_RA7:      
                LATAbits.LATA7 = value;
                break;
            case MC_PIN_RA6:      
                LATAbits.LATA6 = value;
                break;
            case MC_PIN_RC0:      
                LATCbits.LATC0 = value;
                break;
            case MC_PIN_RC1:      
                LATCbits.LATC1 = value;
                break;
            case MC_PIN_RC2:           
                LATCbits.LATC2 = value;
                break;
            case MC_PIN_RC3:           
                LATCbits.LATC3 = value;
                break;
            case MC_PIN_RC4:      
                LATCbits.LATC4 = value;
                break;
            case MC_PIN_RC5:      
                LATCbits.LATC5 = value;
                break;
            case MC_PIN_RC6:      
                LATCbits.LATC6 = value;
                break;
            case MC_PIN_RC7:      
                LATCbits.LATC7 = value;
                break;
            case MC_PIN_19:      
                return -1;
            case MC_PIN_20:      
                return -1;
            case MC_PIN_RB0:      
                LATBbits.LATB0 = value;
                break;
            case MC_PIN_RB1:      
                LATBbits.LATB1 = value;
                break;
            case MC_PIN_RB2:      
                LATBbits.LATB2 = value;
                break;
            case MC_PIN_RB3:      
                LATBbits.LATB3 = value;
                break;
            case MC_PIN_RB4:  
                LATBbits.LATB4 = value;
                break;
            case MC_PIN_RB5:      
                LATBbits.LATB5 = value;
                break;
            case MC_PIN_RB6:      
                LATBbits.LATB6 = value;
                break;
            case MC_PIN_RB7:
                LATBbits.LATB7 = value;
                break;
            default:
                return -1;
        }      
        #endif    /* _18F25K80 */
    }
    return 0;
}

MC_PIN_STATES_E MC_GPIO_getOutputValue(MC_PIN_E pin)
{
    if (MC_pin_getDirection(pin) != MC_PIN_OUTPUT)
    {
        return MC_PIN_STATE_ERROR;
    }
    else if (pin >= MC_PIN_NUM_PINS)
    {
        return MC_PIN_STATE_ERROR;
    }
    else
    {
        #ifdef _18F25K80
        switch(pin)
        {
            case MC_PIN_NULL:
                return MC_PIN_STATE_ERROR;
            case MC_PIN_RE3:
                return MC_PIN_STATE_ERROR;
            case MC_PIN_RA0:
                return (MC_PIN_STATES_E)LATAbits.LATA0;
            case MC_PIN_RA1:
                return (MC_PIN_STATES_E)LATAbits.LATA1;
            case MC_PIN_RA2:       
                return (MC_PIN_STATES_E)LATAbits.LATA2;
            case MC_PIN_RA3:       
                return (MC_PIN_STATES_E)LATAbits.LATA3;
            case MC_PIN_6:       
                return MC_PIN_STATE_ERROR;
            case MC_PIN_RA5:       
                return (MC_PIN_STATES_E)LATAbits.LATA5;
            case MC_PIN_8:
                return MC_PIN_STATE_ERROR;
            case MC_PIN_RA7:      
                return (MC_PIN_STATES_E)LATAbits.LATA7;
            case MC_PIN_RA6:      
                return (MC_PIN_STATES_E)LATAbits.LATA6;
            case MC_PIN_RC0:      
                return (MC_PIN_STATES_E)LATCbits.LATC0;
            case MC_PIN_RC1:      
                return (MC_PIN_STATES_E)LATCbits.LATC1;
            case MC_PIN_RC2:           
                return (MC_PIN_STATES_E)LATCbits.LATC2;
            case MC_PIN_RC3:           
                return (MC_PIN_STATES_E)LATCbits.LATC3;
            case MC_PIN_RC4:      
                return (MC_PIN_STATES_E)LATCbits.LATC4;
            case MC_PIN_RC5:      
                return (MC_PIN_STATES_E)LATCbits.LATC5;
            case MC_PIN_RC6:      
                return (MC_PIN_STATES_E)LATCbits.LATC6;
            case MC_PIN_RC7:      
                return (MC_PIN_STATES_E)LATCbits.LATC7;
            case MC_PIN_19:      
                return MC_PIN_STATE_ERROR;
            case MC_PIN_20:      
                return MC_PIN_STATE_ERROR;
            case MC_PIN_RB0:      
                return (MC_PIN_STATES_E)LATBbits.LATB0;
            case MC_PIN_RB1:      
                return (MC_PIN_STATES_E)LATBbits.LATB1;
            case MC_PIN_RB2:      
                return (MC_PIN_STATES_E)LATBbits.LATB2;
            case MC_PIN_RB3:      
                return (MC_PIN_STATES_E)LATBbits.LATB3;
            case MC_PIN_RB4:       
                return (MC_PIN_STATES_E)LATBbits.LATB4;
            case MC_PIN_RB5:      
                return (MC_PIN_STATES_E)LATBbits.LATB5;
            case MC_PIN_RB6:      
                return (MC_PIN_STATES_E)LATBbits.LATB6;
            case MC_PIN_RB7:
                return (MC_PIN_STATES_E)LATBbits.LATB7;
            default:
                return MC_PIN_STATE_ERROR;
        }
        
        #else
        return MC_PIN_STATE_ERROR;
        #endif    /* _18F25K80 */
    }
}

MC_PIN_STATES_E MC_GPIO_read(MC_PIN_E pin)
{
    if (pin >= MC_PIN_NUM_PINS)
    {
        return MC_PIN_STATE_ERROR;
    }
    else
    {
        #ifdef _18F25K80
        switch(pin)
        {
            case MC_PIN_NULL:
                return MC_PIN_STATE_ERROR;
            case MC_PIN_RE3:
                return MC_PIN_STATE_ERROR;
            case MC_PIN_RA0:
                return (MC_PIN_STATES_E)PORTAbits.RA0;
            case MC_PIN_RA1:
                return (MC_PIN_STATES_E)PORTAbits.RA1;
            case MC_PIN_RA2:       
                return (MC_PIN_STATES_E)PORTAbits.RA2;
            case MC_PIN_RA3:       
                return (MC_PIN_STATES_E)PORTAbits.RA3;
            case MC_PIN_6:       
                return MC_PIN_STATE_ERROR;
            case MC_PIN_RA5:       
                return (MC_PIN_STATES_E)PORTAbits.RA5;
            case MC_PIN_8:
                return MC_PIN_STATE_ERROR;
            case MC_PIN_RA7:      
                return (MC_PIN_STATES_E)PORTAbits.RA7;
            case MC_PIN_RA6:      
                return (MC_PIN_STATES_E)PORTAbits.RA6;
            case MC_PIN_RC0:      
                return (MC_PIN_STATES_E)PORTCbits.RC0;
            case MC_PIN_RC1:      
                return (MC_PIN_STATES_E)PORTCbits.RC1;
            case MC_PIN_RC2:           
                return (MC_PIN_STATES_E)PORTCbits.RC2;
            case MC_PIN_RC3:           
                return (MC_PIN_STATES_E)PORTCbits.RC3;
            case MC_PIN_RC4:      
                return (MC_PIN_STATES_E)PORTCbits.RC4;
            case MC_PIN_RC5:      
                return (MC_PIN_STATES_E)PORTCbits.RC5;
            case MC_PIN_RC6:      
                return (MC_PIN_STATES_E)PORTCbits.RC6;
            case MC_PIN_RC7:      
                return (MC_PIN_STATES_E)PORTCbits.RC7;
            case MC_PIN_19:      
                return MC_PIN_STATE_ERROR;
            case MC_PIN_20:
                return MC_PIN_STATE_ERROR;
            case MC_PIN_RB0:
                return (MC_PIN_STATES_E)PORTBbits.RB0;
            case MC_PIN_RB1:      
                return (MC_PIN_STATES_E)PORTBbits.RB1;
            case MC_PIN_RB2:      
                return (MC_PIN_STATES_E)PORTBbits.RB2;
            case MC_PIN_RB3:      
                return (MC_PIN_STATES_E)PORTBbits.RB3;
            case MC_PIN_RB4:       
                return (MC_PIN_STATES_E)PORTBbits.RB4;
            case MC_PIN_RB5:      
                return (MC_PIN_STATES_E)PORTBbits.RB5;
            case MC_PIN_RB6:      
                return (MC_PIN_STATES_E)PORTBbits.RB6;
            case MC_PIN_RB7:
                return (MC_PIN_STATES_E)PORTBbits.RB7;
            default:
                return MC_PIN_STATE_ERROR;
        }
        
        #else
        return MC_PIN_STATE_ERROR;
        #endif    /* _18F25K80 */
    }
}


uint8_t MC_GPIO_verifyOutputValue(MC_PIN_E pin)
{
    if (MC_GPIO_getOutputValue(pin) == MC_GPIO_read(pin))
    {
        return 1U;
    }
    else
    {
        return 0U;
    }
}