/* 
 * File:   app_network.c
 * Author: Derek Gutheil <dkg8689@rit.edu>
 *
 * Created on October 8, 2016
 */

#include "app_network.h"
#include "app_network_stateMachine.h"
#include "IO_networkManager.h"
#include "app_temp.h"
#include "app_current.h"
#include "app_voltages.h"
 
/* ===========================================================================================================================
 * Public Enumerated type definitions
 * ===========================================================================================================================
*/
typedef enum app_network_GTWStatus_E
{
	APP_NETWORK_GTW_NO_CONTACT  = 0,
	APP_NETWORK_GTW_CONTACT     = 1,
	APP_NETWORK_GTW_TIMEDOUT    = 2,
	APP_NETWORK_GTW_NUM_STATES
} app_network_GTWStatus_E;



/* ===========================================================================================================================
 * Private module variables
 * ===========================================================================================================================
*/
bool networkInitSuccess = false;
app_network_GTWStatus_E networkGTWStatus = APP_NETWORK_GTW_NO_CONTACT;

bool networkGTWTransmitRequested = false;
bool networkGTWQuietRequested = false;

bool networkNegotiate = false;
bool networkTransmit = true;


/* ===========================================================================================================================
 * Private module function declarations
 * ===========================================================================================================================
*/
/* PRIVATE APP DECLARATIONS */

/* ===========================================================================================================================
 *  Module function definitions
 * ===========================================================================================================================
*/
void app_network_init()
{
    //do stuff for the network init state here!
    //such as Initializing CAN communications (HW set up, data structures, etc.)
    
    //This is gonna be tricky, we are going to have to ask the other apps for a data structure that has all the data types they are going to send.
    //Then we will have to store that in some internal structure
    
    //start a timer to use for the GTW timeout
    
    //if this has been done, lets set a flag saying so
    networkInitSuccess = true;
}

void app_network()
{
    //listen for the GTW
    //if (IO_Network_isGTWPresent())
    //{
    networkGTWStatus = APP_NETWORK_GTW_CONTACT;
    //}
    //Check if the timer has expired
    // if it has, set networkGTWStatus to timeout
    // if it has not, check for a message from the GTW
            // If there is a message, reset the timer
            // If there is not, do nothing
    
    
    //check the transmit status
    //if yes:
        //ask the apps for updates
    //if not:
        //do nothing
    
    /*
        //update the requests from GTW...
        //if (IO_Network_transmit_requested())
            networkGTWTransmitRequested = true;
            networkGTWQuietRequested= false;
        //else if (IO_Network_transmit_requested() == false)
            networkGTWTransmitRequested = false;
            networkGTWQuietRequested= true;
    */
}



bool app_network_getInitSuccess()
{
    return networkInitSuccess;
}

bool app_network_isGTWPresent()
{
    if(networkGTWStatus == APP_NETWORK_GTW_CONTACT)
    {
        return true;
    }
    else
    {
        return false;
    }
}

bool app_network_getTransmitRequestStatus()
{
    return networkGTWTransmitRequested;
}

bool app_network_getQuietRequestStatus()
{
    return networkGTWQuietRequested;
}

bool app_network_startNegotiations()
{
    networkNegotiate = true;
    return true;
}

bool app_network_beginTransmitting()
{
    networkTransmit = true;
    return true;
}

bool app_network_stopTransmitting()
{
    networkTransmit = true;
    return true;
}


void app_network_1ms()
{
    if (networkTransmit == true)
    {
        /* GENMSGCYCLETIME = 1 ms */
    }

    return;
}


void app_network_10ms()
{
    if (networkTransmit == true)
    {
        /* GENMSGCYCLETIME = 10 ms */
    }

    return;
}


void app_network_100ms()
{
    if (networkTransmit == true)
    {
        /* GENMSGCYCLETIME = 100 ms */
    }
    

    /* EXAMPLE
     * app_network_BMS0_module_voltages2();
    */
    return;
}


void app_network_1000ms()
{
    if (networkTransmit == true)
    {
        /* GENMSGCYCLETIME = 1000 ms */
    }
    

    /* EXAMPLE
     * app_network_BMS0_module_voltages2();
    */
    return;
}

#ifdef __DEVICE_BMS0__
void app_network_BMS0_temp1()
{
    io_network_BMS0_temp1_S data;
    uint8_t * temperatures;
    temperatures = app_temp_getCellTemps();

    data.cell_temp1 = *(temperatures + 0);
    data.cell_temp2 = *(temperatures + 1);
    data.cell_temp3 = *(temperatures + 2);
    data.cell_temp4 = *(temperatures + 3);
    data.cell_temp5 = *(temperatures + 4);
    data.cell_temp6 = *(temperatures + 5);
    data.cell_temp7 = *(temperatures + 6);
    data.cell_temp8 = *(temperatures + 7);

    io_networkManager_processFrame(IO_NETWORK_BMS0_TEMP1, &data);
    return;
}


void app_network_BMS0_temp2()
{
    io_network_BMS0_temp2_S data;
    uint8_t * temperatures;
    temperatures = app_temp_getCellTemps();

    data.cell_temp9 = *(temperatures + 8);
    data.cell_temp10 = *(temperatures + 9);
    data.cell_temp11 = *(temperatures + 10);
    data.cell_temp12 = *(temperatures + 11);
    data.cell_temp13 = *(temperatures + 12);
    data.cell_temp14 = *(temperatures + 13);
    data.cell_temp15 = *(temperatures + 14);
    //data.bms0_ambient_temp = *(temperatures + 15);

    io_networkManager_processFrame(IO_NETWORK_BMS0_TEMP2, &data);
    return;
}


void app_network_BMS0_module_voltages1() 
{
    io_network_BMS0_module_voltages1_S data;
    uint16_t * cell_voltages;
    cell_voltages = app_voltages_getCellVoltages();

    data.module_v1 = *(cell_voltages + 0);
    data.module_v2 = *(cell_voltages + 1);
    data.module_v3 = *(cell_voltages + 2);
    data.module_v4 = *(cell_voltages + 3);

    io_networkManager_processFrame(IO_NETWORK_BMS0_MODULE_VOLTAGES1, &data);
    return;
}


void app_network_BMS0_module_voltages2() 
{
    io_network_BMS0_module_voltages2_S data;
    uint16_t * cell_voltages;
    cell_voltages = app_voltages_getCellVoltages();

    data.module_v5 = *(cell_voltages + 4);
    data.module_v6 = *(cell_voltages + 5);
    data.module_v7 = *(cell_voltages + 6);
    data.module_v8 = *(cell_voltages + 7);

    io_networkManager_processFrame(IO_NETWORK_BMS0_MODULE_VOLTAGES2, &data);
    return;
}


void app_network_BMS0_module_voltages3() 
{
    io_network_BMS0_module_voltages3_S data;
    uint16_t * cell_voltages;
    cell_voltages = app_voltages_getCellVoltages();

    data.module_v9 = *(cell_voltages + 12);
    data.module_v10 = *(cell_voltages + 13);
    data.module_v11 = *(cell_voltages + 14);
    data.module_v12 = *(cell_voltages + 15);

    io_networkManager_processFrame(IO_NETWORK_BMS0_MODULE_VOLTAGES3, &data);
    return;
}


void app_network_BMS0_module_voltages4() 
{
    io_network_BMS0_module_voltages4_S data;
    uint16_t * cell_voltages;
    cell_voltages = app_voltages_getCellVoltages();

    data.module_v13 = *(cell_voltages + 16);
    data.module_v14 = *(cell_voltages + 17);
    data.module_v15 = *(cell_voltages + 18);
    data.module_v16 = *(cell_voltages + 24);

    io_networkManager_processFrame(IO_NETWORK_BMS0_MODULE_VOLTAGES4, &data);
    return;
}


void app_network_BMS0_module_voltages5() 
{
    io_network_BMS0_module_voltages5_S data;
    uint16_t * cell_voltages;
    cell_voltages = app_voltages_getCellVoltages();

    data.module_v17 = *(cell_voltages + 25);
    data.module_v18 = *(cell_voltages + 26);
    data.module_v19 = *(cell_voltages + 27);
    data.module_v20 = *(cell_voltages + 28);

    io_networkManager_processFrame(IO_NETWORK_BMS0_MODULE_VOLTAGES5, &data);
    return;
}


void app_network_BMS0_module_voltages6() 
{
    io_network_BMS0_module_voltages6_S data;
    uint16_t * cell_voltages;
    cell_voltages = app_voltages_getCellVoltages();

    data.module_v21 = *(cell_voltages + 29);
    data.module_v22 = *(cell_voltages + 30);
    data.module_v23 = *(cell_voltages + 31);
    data.module_v24 = *(cell_voltages + 36);

    io_networkManager_processFrame(IO_NETWORK_BMS0_MODULE_VOLTAGES6, &data);
    return;
}


void app_network_BMS0_module_voltages7() 
{
    io_network_BMS0_module_voltages7_S data;
    uint16_t * cell_voltages;
    cell_voltages = app_voltages_getCellVoltages();

    data.module_v25 = *(cell_voltages + 37);
    data.module_v26 = *(cell_voltages + 38);
    data.module_v27 = *(cell_voltages + 39);
    data.module_v28 = *(cell_voltages + 40);

    io_networkManager_processFrame(IO_NETWORK_BMS0_MODULE_VOLTAGES7, &data);
    return;
}


void app_network_BMS0_module_voltages8() 
{
    io_network_BMS0_module_voltages8_S data;
    uint16_t * cell_voltages;
    cell_voltages = app_voltages_getCellVoltages();

    data.module_v29 = *(cell_voltages + 41);
    data.module_v30 = *(cell_voltages + 42);

    io_networkManager_processFrame(IO_NETWORK_BMS0_MODULE_VOLTAGES8, &data);
    return;
}


void app_network_BMS0_pack_info()
{
    io_network_BMS0_pack_info_S data;

    // CODE

    data.pack_voltage = 0;
    data.pack_current = app_current_getCurrent();
    data.pack_power = 0;
    data.pack_soc = 0;

    io_networkManager_processFrame(IO_NETWORK_BMS0_PACK_INFO, &data);
    return;
}


void app_network_BMS0_module_discharge()
{
    io_network_BMS0_module_discharge_S data;

    // Currently no balancing going on so I'll leave this blank

    return;
}
#endif /* __DEVICE_BMS0__ */

#ifdef __DEVICE_BMS1__
void app_network_BMS1_temp1()
{
    io_network_BMS1_temp1_S data;
    uint8_t * temperatures;
    temperatures = app_temp_getCellTemps();

    data.cell_temp16 = *(temperatures + 0);
    data.cell_temp17 = *(temperatures + 1);
    data.cell_temp18 = *(temperatures + 2);
    data.cell_temp19 = *(temperatures + 3);
    data.cell_temp20 = *(temperatures + 4);
    data.cell_temp21 = *(temperatures + 5);
    data.cell_temp22 = *(temperatures + 6);
    data.cell_temp23 = *(temperatures + 7);

    io_networkManager_processFrame(IO_NETWORK_BMS1_TEMP1, &data);
    return;
}

void app_network_BMS1_temp2()
{
    io_network_BMS1_temp2_S data;
    uint8_t * temperatures;
    temperatures = app_temp_getCellTemps();

    data.cell_temp24 = *(temperatures + 8);
    data.cell_temp25 = *(temperatures + 9);
    data.cell_temp26 = *(temperatures + 10);
    data.cell_temp27 = *(temperatures + 11);
    data.cell_temp28 = *(temperatures + 12);
    data.cell_temp29 = *(temperatures + 13);
    data.cell_temp30 = *(temperatures + 14);
    //data.bms0_ambient_temp = *(temperatures + 15);

    io_networkManager_processFrame(IO_NETWORK_BMS1_TEMP2, &data);
    return;
}


#endif /* __DEVICE_BMS2__ */

#ifdef __DEVICE_BMS2__
void app_network_BMS2_temp1()
{
    io_network_BMS2_temp1_S data;
    uint8_t * temperatures;
    temperatures = app_temp_getCellTemps();

    data.cell_temp31 = *(temperatures + 0);
    data.cell_temp32 = *(temperatures + 1);
    data.cell_temp33 = *(temperatures + 2);
    data.cell_temp34 = *(temperatures + 3);
    data.cell_temp35 = *(temperatures + 4);
    data.cell_temp36 = *(temperatures + 5);
    data.cell_temp37 = *(temperatures + 6);
    data.cell_temp38 = *(temperatures + 7);

    io_networkManager_processFrame(IO_NETWORK_BMS2_TEMP1, &data);
    return;
}

void app_network_BMS2_temp2()
{
    io_network_BMS2_temp2_S data;
    uint8_t * temperatures;
    temperatures = app_temp_getCellTemps();

    data.cell_temp39 = *(temperatures + 8);
    data.cell_temp40 = *(temperatures + 9);
    data.cell_temp41 = *(temperatures + 10);
    data.cell_temp42 = *(temperatures + 11);
    data.cell_temp43 = *(temperatures + 12);
    data.cell_temp44 = *(temperatures + 13);
    data.cell_temp45 = *(temperatures + 14);
    //data.bms0_ambient_temp = *(temperatures + 15);

    io_networkManager_processFrame(IO_NETWORK_BMS2_TEMP2, &data);
    return;
}


#endif /* __DEVICE_BMS2__ */

#ifdef __DEVICE_BMS3__
void app_network_BMS3_temp1()
{
    io_network_BMS3_temp1_S data;
    uint8_t * temperatures;
    temperatures = app_temp_getCellTemps();

    data.cell_temp46 = *(temperatures + 0);
    data.cell_temp47 = *(temperatures + 1);
    data.cell_temp48 = *(temperatures + 2);
    data.cell_temp49 = *(temperatures + 3);
    data.cell_temp50 = *(temperatures + 4);
    data.cell_temp51 = *(temperatures + 5);
    data.cell_temp52 = *(temperatures + 6);
    data.cell_temp53 = *(temperatures + 7);

    io_networkManager_processFrame(IO_NETWORK_BMS3_TEMP1, &data);
    return;
}

void app_network_BMS3_temp2()
{
    io_network_BMS3_temp2_S data;
    uint8_t * temperatures;
    temperatures = app_temp_getCellTemps();

    data.cell_temp54 = *(temperatures + 8);
    data.cell_temp55 = *(temperatures + 9);
    data.cell_temp56 = *(temperatures + 10);
    data.cell_temp57 = *(temperatures + 11);
    data.cell_temp58 = *(temperatures + 12);
    data.cell_temp59 = *(temperatures + 13);
    data.cell_temp60 = *(temperatures + 14);
    //data.bms0_ambient_temp = *(temperatures + 15);

    io_networkManager_processFrame(IO_NETWORK_BMS3_TEMP2, &data);
    return;
}


#endif /* __DEVICE_BMS3__ */