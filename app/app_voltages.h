/*************************************
 *  File - app_voltages.h
 *
 *  Date - 4/21/2017
 *  Author - John Jenco
 *************************************/

#ifndef APP_VOLTAGES_H
#define APP_VOLTAGES_H
 
#include "EVT_std_includes.h"

/* For task manager */
void app_voltages_updateCellVoltages();

/* For network */
uint16_t *app_voltages_getCellVoltages();
 
#endif