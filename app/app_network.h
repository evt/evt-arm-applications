/* 
 * File:   app_network.h
 * Author: Derek Gutheil <dkg8689@rit.edu>
 *
 * Created on October 8, 2016
 */

#ifndef APP_NETWORK_H
#define APP_NETWORK_H

#include <EVT_std_includes.h>

/* ===========================================================================================================================
 * Public Enumerated type definitions
 * ===========================================================================================================================
*/


/* ===========================================================================================================================
 * Public Structure definitions
 * ===========================================================================================================================
*/


/* ===========================================================================================================================
 * Public function declarations
 * ===========================================================================================================================
*/

/*
 * Gets called once by app.c to initialize the application layer of the Network module
*/
void app_network_init();

/*
 * Gets called by a periodic function (100ms) in app.c
 * This is the primary entry point for running the network module
*/
void app_network();

/*
 * Returns true if the module has completed initilization successfuly
*/
bool app_network_getInitSuccess();

/*
 * Returns true if the GTW has been detected on the network
*/
bool app_network_isGTWPresent();

/*
 * Returns true if the GTW has requested devices to transmit data
*/
bool app_network_getTransmitRequestStatus();

/*
 * Returns true if the GTW has requested devices to be quiet
*/
bool app_network_getQuietRequestStatus();

/*
 * Tells the network module to begin network negotiations
*/
bool app_network_startNegotiations();

/*
 * Tells the network module to begin transmitting device data
*/
bool app_network_beginTransmitting();

/*
 * Tells the network module to stop transmitting device data
*/
bool app_network_stopTransmitting();

void app_network_1ms();

void app_network_10ms();

void app_network_100ms();

void app_network_1000ms();


#endif    /* APP_NETWORK_H */