/*
 * File: IO_networkManager.c
 * Created: 4/1/2017
 *
 * Responsible for collecting data from other applications and packaging the
 * data into formatted frames for transmission on the CAN bus.
*/

#include "IO_networkManager.h"
#include "IO_CAN.h"

/* ============================================================================
 * Enumerated Type Definitions
 * ============================================================================
*/


/* ============================================================================
 * Private Module Variables
 * ============================================================================
*/


/* ============================================================================
 * Private Module Function Declarations
 * ============================================================================
*/

#ifdef __DEVICE_GTW__
void package_GTW_requests(io_network_GTW_requests_S * contents, io_can_message_S * message);
#endif /* __DEVICE_GTW__ */

#ifdef __DEVICE_IMU__
void package_IMU_accelerations(io_network_IMU_accelerations_S * contents, io_can_message_S * message);
#endif /* __DEVICE_IMU__ */

#ifdef __DEVICE_IMU__
void package_IMU_angular_positions(io_network_IMU_angular_positions_S * contents, io_can_message_S * message);
#endif /* __DEVICE_IMU__ */

#ifdef __DEVICE_IMU__
void package_IMU_gyro(io_network_IMU_gyro_S * contents, io_can_message_S * message);
#endif /* __DEVICE_IMU__ */

#ifdef __DEVICE_IMU__
void package_IMU_magnetometer(io_network_IMU_magnetometer_S * contents, io_can_message_S * message);
#endif /* __DEVICE_IMU__ */

#ifdef __DEVICE_IMU__
void package_IMU_orientation(io_network_IMU_orientation_S * contents, io_can_message_S * message);
#endif /* __DEVICE_IMU__ */

#ifdef __DEVICE_IMU__
void package_IMU_altimeter(io_network_IMU_altimeter_S * contents, io_can_message_S * message);
#endif /* __DEVICE_IMU__ */

#ifdef __DEVICE_BMS0__
void package_BMS0_module_voltages1(io_network_BMS0_module_voltages1_S * contents, io_can_message_S * message);
#endif /* __DEVICE_BMS0__ */

#ifdef __DEVICE_BMS0__
void package_BMS0_module_voltages2(io_network_BMS0_module_voltages2_S * contents, io_can_message_S * message);
#endif /* __DEVICE_BMS0__ */

#ifdef __DEVICE_BMS0__
void package_BMS0_module_voltages3(io_network_BMS0_module_voltages3_S * contents, io_can_message_S * message);
#endif /* __DEVICE_BMS0__ */

#ifdef __DEVICE_BMS0__
void package_BMS0_module_voltages4(io_network_BMS0_module_voltages4_S * contents, io_can_message_S * message);
#endif /* __DEVICE_BMS0__ */

#ifdef __DEVICE_BMS0__
void package_BMS0_module_voltages5(io_network_BMS0_module_voltages5_S * contents, io_can_message_S * message);
#endif /* __DEVICE_BMS0__ */

#ifdef __DEVICE_BMS0__
void package_BMS0_module_voltages6(io_network_BMS0_module_voltages6_S * contents, io_can_message_S * message);
#endif /* __DEVICE_BMS0__ */

#ifdef __DEVICE_BMS0__
void package_BMS0_module_voltages7(io_network_BMS0_module_voltages7_S * contents, io_can_message_S * message);
#endif /* __DEVICE_BMS0__ */

#ifdef __DEVICE_BMS0__
void package_BMS0_module_voltages8(io_network_BMS0_module_voltages8_S * contents, io_can_message_S * message);
#endif /* __DEVICE_BMS0__ */

#ifdef __DEVICE_BMS0__
void package_BMS0_pack_info(io_network_BMS0_pack_info_S * contents, io_can_message_S * message);
#endif /* __DEVICE_BMS0__ */

#ifdef __DEVICE_BMS0__
void package_BMS0_temp1(io_network_BMS0_temp1_S * contents, io_can_message_S * message);
#endif /* __DEVICE_BMS0__ */

#ifdef __DEVICE_BMS0__
void package_BMS0_temp2(io_network_BMS0_temp2_S * contents, io_can_message_S * message);
#endif /* __DEVICE_BMS0__ */

#ifdef __DEVICE_BMS1__
void package_BMS1_temp1(io_network_BMS1_temp1_S * contents, io_can_message_S * message);
#endif /* __DEVICE_BMS1__ */

#ifdef __DEVICE_BMS1__
void package_BMS1_temp2(io_network_BMS1_temp2_S * contents, io_can_message_S * message);
#endif /* __DEVICE_BMS1__ */

#ifdef __DEVICE_BMS2__
void package_BMS2_temp1(io_network_BMS2_temp1_S * contents, io_can_message_S * message);
#endif /* __DEVICE_BMS2__ */

#ifdef __DEVICE_BMS2__
void package_BMS2_temp2(io_network_BMS2_temp2_S * contents, io_can_message_S * message);
#endif /* __DEVICE_BMS2__ */

#ifdef __DEVICE_BMS3__
void package_BMS3_temp1(io_network_BMS3_temp1_S * contents, io_can_message_S * message);
#endif /* __DEVICE_BMS3__ */

#ifdef __DEVICE_BMS3__
void package_BMS3_temp2(io_network_BMS3_temp2_S * contents, io_can_message_S * message);
#endif /* __DEVICE_BMS3__ */

#ifdef __DEVICE_BMS0__
void package_BMS0_module_discharge(io_network_BMS0_module_discharge_S * contents, io_can_message_S * message);
#endif /* __DEVICE_BMS0__ */


/* ============================================================================
 * Module Function Definitions
 * ============================================================================
*/

/* Initializes the network manager IO layer. */
void io_networkManager_init()
{
    return;
}


/* Instructs the network manager to package a CAN data frame formatted based on
 * a CAN ID with data collected from an application and send it. */
void io_networkManager_processFrame(uint16_t id, void * data)
{
    /* Each case in this switch statement represents a different CAN Message as
     * it is defined in the DBC file. Each case runs a unique function
     * generated for that message to package the bits as defined by that
     * message and sends it to IO_CAN. */
    io_can_message_S message;

    switch (id)
    {
        
#ifdef __DEVICE_GTW__
        case 16:
        {
            package_GTW_requests((io_network_GTW_requests_S *) data, &message);
            break;
        }
#endif /* __DEVICE_GTW__ */

#ifdef __DEVICE_IMU__
        case 1280:
        {
            package_IMU_accelerations((io_network_IMU_accelerations_S *) data, &message);
            break;
        }
#endif /* __DEVICE_IMU__ */

#ifdef __DEVICE_IMU__
        case 1282:
        {
            package_IMU_angular_positions((io_network_IMU_angular_positions_S *) data, &message);
            break;
        }
#endif /* __DEVICE_IMU__ */

#ifdef __DEVICE_IMU__
        case 1283:
        {
            package_IMU_gyro((io_network_IMU_gyro_S *) data, &message);
            break;
        }
#endif /* __DEVICE_IMU__ */

#ifdef __DEVICE_IMU__
        case 1284:
        {
            package_IMU_magnetometer((io_network_IMU_magnetometer_S *) data, &message);
            break;
        }
#endif /* __DEVICE_IMU__ */

#ifdef __DEVICE_IMU__
        case 1285:
        {
            package_IMU_orientation((io_network_IMU_orientation_S *) data, &message);
            break;
        }
#endif /* __DEVICE_IMU__ */

#ifdef __DEVICE_IMU__
        case 1281:
        {
            package_IMU_altimeter((io_network_IMU_altimeter_S *) data, &message);
            break;
        }
#endif /* __DEVICE_IMU__ */

#ifdef __DEVICE_BMS0__
        case 132:
        {
            package_BMS0_module_voltages1((io_network_BMS0_module_voltages1_S *) data, &message);
            break;
        }
#endif /* __DEVICE_BMS0__ */

#ifdef __DEVICE_BMS0__
        case 133:
        {
            package_BMS0_module_voltages2((io_network_BMS0_module_voltages2_S *) data, &message);
            break;
        }
#endif /* __DEVICE_BMS0__ */

#ifdef __DEVICE_BMS0__
        case 134:
        {
            package_BMS0_module_voltages3((io_network_BMS0_module_voltages3_S *) data, &message);
            break;
        }
#endif /* __DEVICE_BMS0__ */

#ifdef __DEVICE_BMS0__
        case 135:
        {
            package_BMS0_module_voltages4((io_network_BMS0_module_voltages4_S *) data, &message);
            break;
        }
#endif /* __DEVICE_BMS0__ */

#ifdef __DEVICE_BMS0__
        case 136:
        {
            package_BMS0_module_voltages5((io_network_BMS0_module_voltages5_S *) data, &message);
            break;
        }
#endif /* __DEVICE_BMS0__ */

#ifdef __DEVICE_BMS0__
        case 137:
        {
            package_BMS0_module_voltages6((io_network_BMS0_module_voltages6_S *) data, &message);
            break;
        }
#endif /* __DEVICE_BMS0__ */

#ifdef __DEVICE_BMS0__
        case 138:
        {
            package_BMS0_module_voltages7((io_network_BMS0_module_voltages7_S *) data, &message);
            break;
        }
#endif /* __DEVICE_BMS0__ */

#ifdef __DEVICE_BMS0__
        case 139:
        {
            package_BMS0_module_voltages8((io_network_BMS0_module_voltages8_S *) data, &message);
            break;
        }
#endif /* __DEVICE_BMS0__ */

#ifdef __DEVICE_BMS0__
        case 130:
        {
            package_BMS0_pack_info((io_network_BMS0_pack_info_S *) data, &message);
            break;
        }
#endif /* __DEVICE_BMS0__ */

#ifdef __DEVICE_BMS0__
        case 128:
        {
            package_BMS0_temp1((io_network_BMS0_temp1_S *) data, &message);
            break;
        }
#endif /* __DEVICE_BMS0__ */

#ifdef __DEVICE_BMS0__
        case 129:
        {
            package_BMS0_temp2((io_network_BMS0_temp2_S *) data, &message);
            break;
        }
#endif /* __DEVICE_BMS0__ */

#ifdef __DEVICE_BMS1__
        case 256:
        {
            package_BMS1_temp1((io_network_BMS1_temp1_S *) data, &message);
            break;
        }
#endif /* __DEVICE_BMS1__ */

#ifdef __DEVICE_BMS1__
        case 257:
        {
            package_BMS1_temp2((io_network_BMS1_temp2_S *) data, &message);
            break;
        }
#endif /* __DEVICE_BMS1__ */

#ifdef __DEVICE_BMS2__
        case 384:
        {
            package_BMS2_temp1((io_network_BMS2_temp1_S *) data, &message);
            break;
        }
#endif /* __DEVICE_BMS2__ */

#ifdef __DEVICE_BMS2__
        case 385:
        {
            package_BMS2_temp2((io_network_BMS2_temp2_S *) data, &message);
            break;
        }
#endif /* __DEVICE_BMS2__ */

#ifdef __DEVICE_BMS3__
        case 512:
        {
            package_BMS3_temp1((io_network_BMS3_temp1_S *) data, &message);
            break;
        }
#endif /* __DEVICE_BMS3__ */

#ifdef __DEVICE_BMS3__
        case 513:
        {
            package_BMS3_temp2((io_network_BMS3_temp2_S *) data, &message);
            break;
        }
#endif /* __DEVICE_BMS3__ */

#ifdef __DEVICE_BMS0__
        case 131:
        {
            package_BMS0_module_discharge((io_network_BMS0_module_discharge_S *) data, &message);
            break;
        }
#endif /* __DEVICE_BMS0__ */

        default:
        {
            /* Error. */
        }
    }

    io_can_transmitMessage(&message);

    return;
}


#ifdef __DEVICE_GTW__
void package_GTW_requests(io_network_GTW_requests_S * contents, io_can_message_S * message)
{
    message->numBytes = 1;
    message->address = 16;

    message->data[0] = (((contents->transmit_request >> 0) << 0)) | (((contents->bms_state_request >> 0) << 1));
}
#endif /* __DEVICE_GTW__ */

#ifdef __DEVICE_IMU__
void package_IMU_accelerations(io_network_IMU_accelerations_S * contents, io_can_message_S * message)
{
    message->numBytes = 7;
    message->address = 1280;

    message->data[0] = (((contents->accel_x >> 0) & 0xff) << 0);
    message->data[1] = (((contents->accel_x >> 8) << 0));
    message->data[2] = (((contents->accel_y >> 0) & 0xff) << 0);
    message->data[3] = (((contents->accel_y >> 8) << 0));
    message->data[4] = (((contents->accel_z >> 0) & 0xff) << 0);
    message->data[5] = (((contents->accel_z >> 8) << 0));
    message->data[6] = (((contents->shake_accel >> 0) << 0));
}
#endif /* __DEVICE_IMU__ */

#ifdef __DEVICE_IMU__
void package_IMU_angular_positions(io_network_IMU_angular_positions_S * contents, io_can_message_S * message)
{
    message->numBytes = 6;
    message->address = 1282;

    message->data[0] = (((contents->pitch_y >> 0) & 0xff) << 0);
    message->data[1] = (((contents->pitch_y >> 8) << 0));
    message->data[2] = (((contents->roll_x >> 0) & 0xff) << 0);
    message->data[3] = (((contents->roll_x >> 8) << 0));
    message->data[4] = (((contents->yaw_z >> 0) & 0xff) << 0);
    message->data[5] = (((contents->yaw_z >> 8) << 0));
}
#endif /* __DEVICE_IMU__ */

#ifdef __DEVICE_IMU__
void package_IMU_gyro(io_network_IMU_gyro_S * contents, io_can_message_S * message)
{
    message->numBytes = 7;
    message->address = 1283;

    message->data[0] = (((contents->gyro_x >> 0) & 0xff) << 0);
    message->data[1] = (((contents->gyro_x >> 8) << 0));
    message->data[2] = (((contents->gyro_y >> 0) & 0xff) << 0);
    message->data[3] = (((contents->gyro_y >> 8) << 0));
    message->data[4] = (((contents->gyro_z >> 0) & 0xff) << 0);
    message->data[5] = (((contents->gyro_z >> 8) << 0));
    message->data[6] = (((contents->shake_gyro >> 0) << 0));
}
#endif /* __DEVICE_IMU__ */

#ifdef __DEVICE_IMU__
void package_IMU_magnetometer(io_network_IMU_magnetometer_S * contents, io_can_message_S * message)
{
    message->numBytes = 8;
    message->address = 1284;

    message->data[0] = (((contents->flux_x >> 0) & 0xff) << 0);
    message->data[1] = (((contents->flux_x >> 8) << 0));
    message->data[2] = (((contents->flux_y >> 0) & 0xff) << 0);
    message->data[3] = (((contents->flux_y >> 8) << 0));
    message->data[4] = (((contents->flux_z >> 0) & 0xff) << 0);
    message->data[5] = (((contents->flux_z >> 8) << 0));
    message->data[6] = (((contents->north >> 0) & 0xff) << 0);
    message->data[7] = (((contents->north >> 8) << 0));
}
#endif /* __DEVICE_IMU__ */

#ifdef __DEVICE_IMU__
void package_IMU_orientation(io_network_IMU_orientation_S * contents, io_can_message_S * message)
{
    message->numBytes = 8;
    message->address = 1285;

    message->data[0] = (((contents->orientation_x >> 0) & 0xff) << 0);
    message->data[1] = (((contents->orientation_x >> 8) << 0));
    message->data[2] = (((contents->orientation_y >> 0) & 0xff) << 0);
    message->data[3] = (((contents->orientation_y >> 8) << 0));
    message->data[4] = (((contents->orientation_z >> 0) & 0xff) << 0);
    message->data[5] = (((contents->orientation_z >> 8) << 0));
    message->data[6] = (((contents->orientation_w >> 0) & 0xff) << 0);
    message->data[7] = (((contents->orientation_w >> 8) << 0));
}
#endif /* __DEVICE_IMU__ */

#ifdef __DEVICE_IMU__
void package_IMU_altimeter(io_network_IMU_altimeter_S * contents, io_can_message_S * message)
{
    message->numBytes = 4;
    message->address = 1281;

    message->data[0] = (((contents->altitude >> 0) & 0xff) << 0);
    message->data[1] = (((contents->altitude >> 8) & 0xff) << 0);
    message->data[2] = (((contents->altitude >> 16) << 0)) | (((contents->imu_ambient_temp >> 0) & 0x0f) << 4);
    message->data[3] = (((contents->imu_ambient_temp >> 4) << 0));
}
#endif /* __DEVICE_IMU__ */

#ifdef __DEVICE_BMS0__
void package_BMS0_module_voltages1(io_network_BMS0_module_voltages1_S * contents, io_can_message_S * message)
{
    message->numBytes = 8;
    message->address = 132;

    message->data[0] = (((contents->module_v1 >> 0) & 0xff) << 0);
    message->data[1] = (((contents->module_v1 >> 8) << 0));
    message->data[2] = (((contents->module_v2 >> 0) & 0xff) << 0);
    message->data[3] = (((contents->module_v2 >> 8) << 0));
    message->data[4] = (((contents->module_v3 >> 0) & 0xff) << 0);
    message->data[5] = (((contents->module_v3 >> 8) << 0));
    message->data[6] = (((contents->module_v4 >> 0) & 0xff) << 0);
    message->data[7] = (((contents->module_v4 >> 8) << 0));
}
#endif /* __DEVICE_BMS0__ */

#ifdef __DEVICE_BMS0__
void package_BMS0_module_voltages2(io_network_BMS0_module_voltages2_S * contents, io_can_message_S * message)
{
    message->numBytes = 8;
    message->address = 133;

    message->data[0] = (((contents->module_v5 >> 0) & 0xff) << 0);
    message->data[1] = (((contents->module_v5 >> 8) << 0));
    message->data[2] = (((contents->module_v6 >> 0) & 0xff) << 0);
    message->data[3] = (((contents->module_v6 >> 8) << 0));
    message->data[4] = (((contents->module_v7 >> 0) & 0xff) << 0);
    message->data[5] = (((contents->module_v7 >> 8) << 0));
    message->data[6] = (((contents->module_v8 >> 0) & 0xff) << 0);
    message->data[7] = (((contents->module_v8 >> 8) << 0));
}
#endif /* __DEVICE_BMS0__ */

#ifdef __DEVICE_BMS0__
void package_BMS0_module_voltages3(io_network_BMS0_module_voltages3_S * contents, io_can_message_S * message)
{
    message->numBytes = 8;
    message->address = 134;

    message->data[0] = (((contents->module_v9 >> 0) & 0xff) << 0);
    message->data[1] = (((contents->module_v9 >> 8) << 0));
    message->data[2] = (((contents->module_v10 >> 0) & 0xff) << 0);
    message->data[3] = (((contents->module_v10 >> 8) << 0));
    message->data[4] = (((contents->module_v11 >> 0) & 0xff) << 0);
    message->data[5] = (((contents->module_v11 >> 8) << 0));
    message->data[6] = (((contents->module_v12 >> 0) & 0xff) << 0);
    message->data[7] = (((contents->module_v12 >> 8) << 0));
}
#endif /* __DEVICE_BMS0__ */

#ifdef __DEVICE_BMS0__
void package_BMS0_module_voltages4(io_network_BMS0_module_voltages4_S * contents, io_can_message_S * message)
{
    message->numBytes = 8;
    message->address = 135;

    message->data[0] = (((contents->module_v13 >> 0) & 0xff) << 0);
    message->data[1] = (((contents->module_v13 >> 8) << 0));
    message->data[2] = (((contents->module_v14 >> 0) & 0xff) << 0);
    message->data[3] = (((contents->module_v14 >> 8) << 0));
    message->data[4] = (((contents->module_v15 >> 0) & 0xff) << 0);
    message->data[5] = (((contents->module_v15 >> 8) << 0));
    message->data[6] = (((contents->module_v16 >> 0) & 0xff) << 0);
    message->data[7] = (((contents->module_v16 >> 8) << 0));
}
#endif /* __DEVICE_BMS0__ */

#ifdef __DEVICE_BMS0__
void package_BMS0_module_voltages5(io_network_BMS0_module_voltages5_S * contents, io_can_message_S * message)
{
    message->numBytes = 8;
    message->address = 136;

    message->data[0] = (((contents->module_v17 >> 0) & 0xff) << 0);
    message->data[1] = (((contents->module_v17 >> 8) << 0));
    message->data[2] = (((contents->module_v18 >> 0) & 0xff) << 0);
    message->data[3] = (((contents->module_v18 >> 8) << 0));
    message->data[4] = (((contents->module_v19 >> 0) & 0xff) << 0);
    message->data[5] = (((contents->module_v19 >> 8) << 0));
    message->data[6] = (((contents->module_v20 >> 0) & 0xff) << 0);
    message->data[7] = (((contents->module_v20 >> 8) << 0));
}
#endif /* __DEVICE_BMS0__ */

#ifdef __DEVICE_BMS0__
void package_BMS0_module_voltages6(io_network_BMS0_module_voltages6_S * contents, io_can_message_S * message)
{
    message->numBytes = 8;
    message->address = 137;

    message->data[0] = (((contents->module_v21 >> 0) & 0xff) << 0);
    message->data[1] = (((contents->module_v21 >> 8) << 0));
    message->data[2] = (((contents->module_v22 >> 0) & 0xff) << 0);
    message->data[3] = (((contents->module_v22 >> 8) << 0));
    message->data[4] = (((contents->module_v23 >> 0) & 0xff) << 0);
    message->data[5] = (((contents->module_v23 >> 8) << 0));
    message->data[6] = (((contents->module_v24 >> 0) & 0xff) << 0);
    message->data[7] = (((contents->module_v24 >> 8) << 0));
}
#endif /* __DEVICE_BMS0__ */

#ifdef __DEVICE_BMS0__
void package_BMS0_module_voltages7(io_network_BMS0_module_voltages7_S * contents, io_can_message_S * message)
{
    message->numBytes = 8;
    message->address = 138;

    message->data[0] = (((contents->module_v25 >> 0) & 0xff) << 0);
    message->data[1] = (((contents->module_v25 >> 8) << 0));
    message->data[2] = (((contents->module_v26 >> 0) & 0xff) << 0);
    message->data[3] = (((contents->module_v26 >> 8) << 0));
    message->data[4] = (((contents->module_v27 >> 0) & 0xff) << 0);
    message->data[5] = (((contents->module_v27 >> 8) << 0));
    message->data[6] = (((contents->module_v28 >> 0) & 0xff) << 0);
    message->data[7] = (((contents->module_v28 >> 8) << 0));
}
#endif /* __DEVICE_BMS0__ */

#ifdef __DEVICE_BMS0__
void package_BMS0_module_voltages8(io_network_BMS0_module_voltages8_S * contents, io_can_message_S * message)
{
    message->numBytes = 4;
    message->address = 139;

    message->data[0] = (((contents->module_v29 >> 0) & 0xff) << 0);
    message->data[1] = (((contents->module_v29 >> 8) << 0));
    message->data[2] = (((contents->module_v30 >> 0) & 0xff) << 0);
    message->data[3] = (((contents->module_v30 >> 8) << 0));
}
#endif /* __DEVICE_BMS0__ */

#ifdef __DEVICE_BMS0__
void package_BMS0_pack_info(io_network_BMS0_pack_info_S * contents, io_can_message_S * message)
{
    message->numBytes = 7;
    message->address = 130;

    message->data[0] = (((contents->pack_voltage >> 0) & 0xff) << 0);
    message->data[1] = (((contents->pack_voltage >> 8) << 0)) | (((contents->pack_current >> 0) & 0x3f) << 2);
    message->data[2] = (((contents->pack_current >> 6) & 0xff) << 0);
    message->data[3] = (((contents->pack_current >> 14) << 0)) | (((contents->pack_power >> 0) & 0x3f) << 2);
    message->data[4] = (((contents->pack_power >> 6) & 0xff) << 0);
    message->data[5] = (((contents->pack_power >> 14) << 0)) | (((contents->pack_soc >> 0) & 0x03) << 6);
    message->data[6] = (((contents->pack_soc >> 2) << 0));
}
#endif /* __DEVICE_BMS0__ */

#ifdef __DEVICE_BMS0__
void package_BMS0_temp1(io_network_BMS0_temp1_S * contents, io_can_message_S * message)
{
    message->numBytes = 8;
    message->address = 128;

    message->data[0] = (((contents->cell_temp1 >> 0) << 0));
    message->data[1] = (((contents->cell_temp2 >> 0) << 0));
    message->data[2] = (((contents->cell_temp3 >> 0) << 0));
    message->data[3] = (((contents->cell_temp4 >> 0) << 0));
    message->data[4] = (((contents->cell_temp5 >> 0) << 0));
    message->data[5] = (((contents->cell_temp6 >> 0) << 0));
    message->data[6] = (((contents->cell_temp7 >> 0) << 0));
    message->data[7] = (((contents->cell_temp8 >> 0) << 0));
}
#endif /* __DEVICE_BMS0__ */

#ifdef __DEVICE_BMS0__
void package_BMS0_temp2(io_network_BMS0_temp2_S * contents, io_can_message_S * message)
{
    message->numBytes = 8;
    message->address = 129;

    message->data[0] = (((contents->cell_temp9 >> 0) << 0));
    message->data[1] = (((contents->cell_temp10 >> 0) << 0));
    message->data[2] = (((contents->cell_temp11 >> 0) << 0));
    message->data[3] = (((contents->cell_temp12 >> 0) << 0));
    message->data[4] = (((contents->cell_temp13 >> 0) << 0));
    message->data[5] = (((contents->cell_temp14 >> 0) << 0));
    message->data[6] = (((contents->cell_temp15 >> 0) << 0));
    message->data[7] = (((contents->bms0_ambient_temp >> 0) << 0));
}
#endif /* __DEVICE_BMS0__ */

#ifdef __DEVICE_BMS1__
void package_BMS1_temp1(io_network_BMS1_temp1_S * contents, io_can_message_S * message)
{
    message->numBytes = 8;
    message->address = 256;

    message->data[0] = (((contents->cell_temp16 >> 0) << 0));
    message->data[1] = (((contents->cell_temp17 >> 0) << 0));
    message->data[2] = (((contents->cell_temp18 >> 0) << 0));
    message->data[3] = (((contents->cell_temp19 >> 0) << 0));
    message->data[4] = (((contents->cell_temp20 >> 0) << 0));
    message->data[5] = (((contents->cell_temp21 >> 0) << 0));
    message->data[6] = (((contents->cell_temp22 >> 0) << 0));
    message->data[7] = (((contents->cell_temp23 >> 0) << 0));
}
#endif /* __DEVICE_BMS1__ */

#ifdef __DEVICE_BMS1__
void package_BMS1_temp2(io_network_BMS1_temp2_S * contents, io_can_message_S * message)
{
    message->numBytes = 8;
    message->address = 257;

    message->data[0] = (((contents->cell_temp24 >> 0) << 0));
    message->data[1] = (((contents->cell_temp25 >> 0) << 0));
    message->data[2] = (((contents->cell_temp26 >> 0) << 0));
    message->data[3] = (((contents->cell_temp27 >> 0) << 0));
    message->data[4] = (((contents->cell_temp28 >> 0) << 0));
    message->data[5] = (((contents->cell_temp29 >> 0) << 0));
    message->data[6] = (((contents->cell_temp30 >> 0) << 0));
    message->data[7] = (((contents->bms1_ambient_temp >> 0) << 0));
}
#endif /* __DEVICE_BMS1__ */

#ifdef __DEVICE_BMS2__
void package_BMS2_temp1(io_network_BMS2_temp1_S * contents, io_can_message_S * message)
{
    message->numBytes = 8;
    message->address = 384;

    message->data[0] = (((contents->cell_temp31 >> 0) << 0));
    message->data[1] = (((contents->cell_temp32 >> 0) << 0));
    message->data[2] = (((contents->cell_temp33 >> 0) << 0));
    message->data[3] = (((contents->cell_temp34 >> 0) << 0));
    message->data[4] = (((contents->cell_temp35 >> 0) << 0));
    message->data[5] = (((contents->cell_temp36 >> 0) << 0));
    message->data[6] = (((contents->cell_temp37 >> 0) << 0));
    message->data[7] = (((contents->cell_temp38 >> 0) << 0));
}
#endif /* __DEVICE_BMS2__ */

#ifdef __DEVICE_BMS2__
void package_BMS2_temp2(io_network_BMS2_temp2_S * contents, io_can_message_S * message)
{
    message->numBytes = 8;
    message->address = 385;

    message->data[0] = (((contents->cell_temp39 >> 0) << 0));
    message->data[1] = (((contents->cell_temp40 >> 0) << 0));
    message->data[2] = (((contents->cell_temp41 >> 0) << 0));
    message->data[3] = (((contents->cell_temp42 >> 0) << 0));
    message->data[4] = (((contents->cell_temp43 >> 0) << 0));
    message->data[5] = (((contents->cell_temp44 >> 0) << 0));
    message->data[6] = (((contents->cell_temp45 >> 0) << 0));
    message->data[7] = (((contents->bms2_ambient_temp >> 0) << 0));
}
#endif /* __DEVICE_BMS2__ */

#ifdef __DEVICE_BMS3__
void package_BMS3_temp1(io_network_BMS3_temp1_S * contents, io_can_message_S * message)
{
    message->numBytes = 8;
    message->address = 512;

    message->data[0] = (((contents->cell_temp46 >> 0) << 0));
    message->data[1] = (((contents->cell_temp47 >> 0) << 0));
    message->data[2] = (((contents->cell_temp48 >> 0) << 0));
    message->data[3] = (((contents->cell_temp49 >> 0) << 0));
    message->data[4] = (((contents->cell_temp50 >> 0) << 0));
    message->data[5] = (((contents->cell_temp51 >> 0) << 0));
    message->data[6] = (((contents->cell_temp52 >> 0) << 0));
    message->data[7] = (((contents->cell_temp53 >> 0) << 0));
}
#endif /* __DEVICE_BMS3__ */

#ifdef __DEVICE_BMS3__
void package_BMS3_temp2(io_network_BMS3_temp2_S * contents, io_can_message_S * message)
{
    message->numBytes = 8;
    message->address = 513;

    message->data[0] = (((contents->cell_temp54 >> 0) << 0));
    message->data[1] = (((contents->cell_temp55 >> 0) << 0));
    message->data[2] = (((contents->cell_temp56 >> 0) << 0));
    message->data[3] = (((contents->cell_temp57 >> 0) << 0));
    message->data[4] = (((contents->cell_temp58 >> 0) << 0));
    message->data[5] = (((contents->cell_temp59 >> 0) << 0));
    message->data[6] = (((contents->cell_temp60 >> 0) << 0));
    message->data[7] = (((contents->bms3_ambient_temp >> 0) << 0));
}
#endif /* __DEVICE_BMS3__ */

#ifdef __DEVICE_BMS0__
void package_BMS0_module_discharge(io_network_BMS0_module_discharge_S * contents, io_can_message_S * message)
{
    message->numBytes = 4;
    message->address = 131;

    message->data[0] = (((contents->module_discharge_1 >> 0) << 0)) | (((contents->module_discharge_2 >> 0) << 1)) | (((contents->module_discharge_3 >> 0) << 2)) | (((contents->module_discharge_4 >> 0) << 3)) | (((contents->module_discharge_5 >> 0) << 4)) | (((contents->module_discharge_6 >> 0) << 5)) | (((contents->module_discharge_7 >> 0) << 6)) | (((contents->module_discharge_8 >> 0) << 7));
    message->data[1] = (((contents->module_discharge_9 >> 0) << 0)) | (((contents->module_discharge_10 >> 0) << 1)) | (((contents->module_discharge_11 >> 0) << 2)) | (((contents->module_discharge_12 >> 0) << 3)) | (((contents->module_discharge_13 >> 0) << 4)) | (((contents->module_discharge_14 >> 0) << 5)) | (((contents->module_discharge_15 >> 0) << 6)) | (((contents->module_discharge_16 >> 0) << 7));
    message->data[2] = (((contents->module_discharge_17 >> 0) << 0)) | (((contents->module_discharge_18 >> 0) << 1)) | (((contents->module_discharge_19 >> 0) << 2)) | (((contents->module_discharge_20 >> 0) << 3)) | (((contents->module_discharge_21 >> 0) << 4)) | (((contents->module_discharge_22 >> 0) << 5)) | (((contents->module_discharge_23 >> 0) << 6)) | (((contents->module_discharge_24 >> 0) << 7));
    message->data[3] = (((contents->module_discharge_25 >> 0) << 0)) | (((contents->module_discharge_26 >> 0) << 1)) | (((contents->module_discharge_27 >> 0) << 2)) | (((contents->module_discharge_28 >> 0) << 3)) | (((contents->module_discharge_29 >> 0) << 4)) | (((contents->module_discharge_30 >> 0) << 5));
}
#endif /* __DEVICE_BMS0__ */
