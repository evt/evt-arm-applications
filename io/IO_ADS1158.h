/*
 * File:    IO_ADS1158.h
 * Created: 3/31/2017
*/

#ifndef IO_ADS1158_H
#define IO_ADS1158_H

#include "EVT_std_includes.h"

/* ===========================================================================================================================
 * Public precompiler definitions
 * ===========================================================================================================================
*/


/* ===========================================================================================================================
 * Public Enumerated type definitions
 * ===========================================================================================================================
*/

//TODO: Fix these names (naming convention)

/*channel IDs (see pg 32 of Datasheet) */
typedef enum ads1158_channel_E
{
    ADS1158_CHANNEL_ID_DIFF0  = 0u,
    ADS1158_CHANNEL_ID_DIFF1  = 1u,
    ADS1158_CHANNEL_ID_DIFF2  = 2u,
    ADS1158_CHANNEL_ID_DIFF3  = 3u,
    ADS1158_CHANNEL_ID_DIFF4  = 4u,
    ADS1158_CHANNEL_ID_DIFF5  = 5u,
    ADS1158_CHANNEL_ID_DIFF6  = 6u,
    ADS1158_CHANNEL_ID_DIFF7  = 7u,
    ADS1158_CHANNEL_ID_AIN0   = 8u,
    ADS1158_CHANNEL_ID_AIN1   = 9u,
    ADS1158_CHANNEL_ID_AIN2   = 10u,
    ADS1158_CHANNEL_ID_AIN3   = 11u,
    ADS1158_CHANNEL_ID_AIN4   = 12u,
    ADS1158_CHANNEL_ID_AIN5   = 13u,
    ADS1158_CHANNEL_ID_AIN6   = 14u,
    ADS1158_CHANNEL_ID_AIN7   = 15u,
    ADS1158_CHANNEL_ID_AIN8   = 16u,
    ADS1158_CHANNEL_ID_AIN9   = 17u,
    ADS1158_CHANNEL_ID_AIN10  = 18u,
    ADS1158_CHANNEL_ID_AIN11  = 19u,
    ADS1158_CHANNEL_ID_AIN12  = 20u,
    ADS1158_CHANNEL_ID_AIN13  = 21u,
    ADS1158_CHANNEL_ID_AIN14  = 22u,
    ADS1158_CHANNEL_ID_AIN15  = 23u,
    ADS1158_CHANNEL_ID_OFFSET = 24u,
    ADS1158_CHANNEL_ID_NULL   = 25u,   
    ADS1158_CHANNEL_ID_VCC    = 26u, //Intentionally skips 25
    ADS1158_CHANNEL_ID_TEMP   = 27u,
    ADS1158_CHANNEL_ID_GAIN   = 28u,
    ADS1158_CHANNEL_ID_REF    = 29u,
    ADS1158_NUM_CHANNEL_IDS,
} ads1158_channel_E;

/*GPIO IDs */
typedef enum ads1158_GPIO_E
{
    ADS1158_GPIO_0  = 0u,
    ADS1158_GPIO_1  = 1u,
    ADS1158_GPIO_2  = 2u,
    ADS1158_GPIO_3  = 3u,
    ADS1158_GPIO_4  = 4u,
    ADS1158_GPIO_5  = 5u,
    ADS1158_GPIO_6  = 6u,
    ADS1158_GPIO_7  = 7u,
    ADS1158_NUM_GPIO,
    ADS1158_GPIO_LED   = ADS1158_GPIO_4,
    ADS1158_GPIO_START = ADS1158_GPIO_3,
} ads1158_GPIO_E;

typedef enum ads1158_GPIO_direction_E
{
    ADS1158_GPIO_DIRECTION_DEFAULT  = 1u,
    ADS1158_GPIO_DIRECTION_OUTPUT   = 0u, // GPIO is an output
    ADS1158_GPIO_DIRECTION_INPUT    = 1u, // GPIO is an input (Default)
} ads1158_GPIO_direction_E;

typedef enum ads1158_GPIO_value_E
{
    ADS1158_GPIO_VALUE_DEFAULT  = 0u,
    ADS1158_GPIO_VALUE_LOW      = 0u, // GPIO is logic low (default)
    ADS1158_GPIO_VALUE_HIGH     = 1u, // GPIO is logic high
} ads1158_GPIO_value_E;

typedef enum ads1158_state_E
{
    ADS1158_STATE_RESET_SPI,
    ADS1158_STATE_RESET,
    ADS1158_STATE_SETUP_CONFIG,
    ADS1158_STATE_READ_DATA,
    ADS1158_STATE_ERROR,
} ads1158_state_E;

typedef enum ads1158_errors_E
{
    ADS1158_NO_ERRORS,
    //Populate with errors
} ads1158_errors_E;

typedef enum ads1158_read_precision_E
{
    ADS1158_READ_PRECISION_MICROVOLTS,
    ADS1158_READ_PRECISION_MILLIVOLTS,
} ads1158_read_precision_E;

/* ===========================================================================================================================
 * Public Structure definitions
 * ===========================================================================================================================
*/


/* ===========================================================================================================================
 * Public Type definitions
 * ===========================================================================================================================
*/

/* ===========================================================================================================================
 * Public function declarations
 * ===========================================================================================================================
*/

void io_ads1158_init();

void io_ads1158_run();

ads1158_state_E io_ads1158_getState();

void io_ads1158_enableChannel(ads1158_channel_E channel);

void io_ads1158_disableChannel(ads1158_channel_E channel);

void io_ads1158_setGPIODirection(ads1158_GPIO_E GPIO, ads1158_GPIO_direction_E direction);

void io_ads1158_GPIOwrite(ads1158_GPIO_E GPIO, ads1158_GPIO_value_E value);

ads1158_GPIO_value_E io_ads1158_GPIOread(ads1158_GPIO_E GPIO);

uint16_t * io_ads1158_getMilliVoltChannels();

uint32_t io_ads1158_getMicroVoltChannel();

//void io_ads1158_getAllChannelVoltages(io_ads1158_voltage_T * channelVoltages);



void io_ads1158_changePrecision(ads1158_read_precision_E precision);

/***********************************************************************************
* Name        : 
* Description : runs a bunch of initialization commands for the ADC
* Pre         : SPI is initialized
* @param      : None
* @return     : ADC is initialized
***********************************************************************************/

#endif 	/* IO_ADS1158_H */