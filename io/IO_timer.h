/* 
 * File:   IO_timer.h
 * Author: Derek Gutheil <dkg8689@rit.edu>
 *
 * Created on September 9, 2016, 8:07 PM
 */

#ifndef IO_TIMER_H
#define IO_TIMER_H

typedef enum io_timer_E
{
/* This matches an MC layer ENUM.
 * Do NOT change this without changing both
 */
  IO_TIMER_1,
  IO_TIMER_2,
  IO_TIMER_3,
  IO_TIMER_4,
  NUM_IO_TIMERS
} io_timer_E;

typedef enum io_timer_interrupt_periods_E
{
/* This matches an MC layer ENUM.
 * Do NOT change this without changing both
 */
  IO_TIMER_100US,
  IO_TIMER_200US,
  IO_TIMER_300US,
  IO_TIMER_400US,
  IO_TIMER_500US,
  IO_TIMER_600US,
  IO_TIMER_700US,
  IO_TIMER_800US,
  IO_TIMER_900US,
  IO_TIMER_1MS,
  IO_TIMER_2MS,
  IO_TIMER_4MS,
  IO_TIMER_8MS,
  IO_TIMER_16MS,
} io_timer_interrupt_periods_E;



/* Name: 
 *      IO_timer_init
 * Parameters: 
 *      - timer: The enum of the timer to set up
 *      - interrupt_value: Once the timer hits this value, it will trigger an interrupt.
 *                         The Timer ticks every 4 micro seconds (with an 8 MHz clock), so to figure out the real time that your interrupt will occur at use this formula:
 *                         Interrupt frequency = interrupt_value * 4 us
 *                         ex: 1ms = 250 * 4us
 * Description:
 *      Set up a timer. This sets up and turns on a timer.
 * */
void io_timer_init(io_timer_E timer, io_timer_interrupt_periods_E interrupt_value);

#endif    /* IO_TIMER_H */
